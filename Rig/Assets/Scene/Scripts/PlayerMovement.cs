﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    private Rigidbody rigidBody;
    private Animator animator;
    private float hInput;
    private float vInput;
    private float runInput;
    private float currentspeed = 0;
    public float walkspeed = 3;
    public float runspeed = 6;
    public float turnSmoothing = 20.0f;

    public float idleWaitingTime = 5f;
    private float currentWaitingTime = 0;
    void Start () {

        rigidBody = GetComponent<Rigidbody>();

        if (rigidBody == null)
        {
            Debug.Log("No rigidbody component found in this gameobject.");
            enabled = false ;
            return;
        }
        animator = GetComponent<Animator>();

        if (animator == null)
        {
            Debug.Log("No animator component found in this gameobject.");
            enabled = false;
            return;
        }
      }
	
	void Update () {

        vInput = Input.GetAxis("Horizontal");
        hInput = Input.GetAxis("Vertical");
        runInput = Input.GetAxis("Run");

        handleRotation();

        if (currentspeed == 0)
        {
            currentWaitingTime += Time.deltaTime; 
        }
        else
        {
            currentWaitingTime = 0;
        }
        }
    private void OnAnimatorMove()
    {
        animator.SetFloat("Speed", currentspeed);

        if (currentWaitingTime >= idleWaitingTime)
        {
            if (Random.Range(0, 99) < 30)
            {
                animator.SetTrigger("Idle1");
            }
            currentWaitingTime = 0;
        }
        
    }

    private void FixedUpdate()
    {
        handleMovement();
         
    }
    private void handleMovement() {
        float targetspeed = walkspeed;

        if (runInput > 0)
            targetspeed = runspeed;

        if (hInput != 0 || vInput != 0)
        {
            rigidBody.velocity = new Vector3(hInput, 0, vInput) * targetspeed;
        }
        else
        {
            rigidBody.velocity = Vector3.zero;
        }
        currentspeed = rigidBody.velocity.magnitude;
    }
    private  void handleRotation()
    {
         if (hInput != 0 || vInput != 0)
    {
        Quaternion newRotation = Quaternion.LookRotation(new Vector3(hInput, 0, vInput));
        transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime * turnSmoothing);
    }
    }
}